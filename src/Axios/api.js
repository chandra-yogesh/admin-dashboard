import axios from "axios";

const apiAxios = axios.create({
    baseURL: 'http://3.110.167.147/api',
  });

  export default apiAxios;