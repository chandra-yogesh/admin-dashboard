import "./newRoom.scss";
import Sidebar from "../../components/sidebar/Sidebar";
import Navbar from "../../components/navbar/Navbar";
import DriveFolderUploadOutlinedIcon from "@mui/icons-material/DriveFolderUploadOutlined";
import { useState } from "react";
import { roomInputs } from "../../formSource";
import useFetch from "../../hooks/useFetch";
import apiAxios from "../../Axios/api";
import swal from "sweetalert";
import { useNavigate } from "react-router-dom";

const NewRoom = () => {
  const [info, setInfo] = useState({
    name: "",
    desc: "",
    pricePerNight: "",
    guestCapacity: "",
    photo: ""
  });
  const [hotelId, setHotelId] = useState(undefined);
  const [rooms, setRooms] = useState([]);
  const [files, setFiles] = useState("");
  const [alert, setAlert] = useState("")
  const navigate = useNavigate();
  const user = JSON.parse(localStorage.getItem("user"))
  const token = user.token
  console.log(token)


  // const { data, loading, error } = useFetch("/room");

  const onFileChange = (e) =>{

    if(e.target.files[0].size < 5000000){
      setAlert("")
      setFiles(e.target.files); setInfo((prevState)=>{
        return {
          ...prevState,
          photo: e.target.files[0]
        }
      });
    }else{
      setAlert("File size should be less than 5Mb")
    }
     console.log(info.photo)
  
  }
  

  const handleClick = async (e) => {
    e.preventDefault();
    const formData = new FormData()
    formData.append('name', info.name) 
    formData.append('desc', info.desc) 
    formData.append('pricePerNight', info.pricePerNight) 
    formData.append('guestCapacity', info.guestCapacity) 
    formData.append('photo', info.photo) 
    // const roomNumbers = rooms.split(",").map((room) => ({ number: room }));
console.log(info)
    if(info.name.trim() === '' || info.desc.trim() === '' || info.pricePerNight.trim() === '' || info.guestCapacity.trim() === '' || info.photo === '' ){
      swal({
        title: "Error",
        icon: 'warning',
        text: 'Please fill all required field',
      })
    }

    else{
        const config = {
          headers: {
            'content-type': 'multipart/form-data',
            'Authorization': `Bearer ${token}`
          }}
        try {
          
          await apiAxios.post(`/room`, formData, config)
          .then((res)=>{
            swal({
              title: "Success",
              icon: "success",
              text: "New Room is added",
            })
            navigate('/room')

          })
        } catch (err) {
          console.log(err);
          swal({
            title: "Error",
            icon: "danger",
            text: "There is an error creating new room",
          })
        }
    }
    
  }

  console.log(info)
  return (
    <div className="new">
      <Sidebar />
      <div className="newContainer">
        <Navbar />
        <div className="top">
          <h1>Add New Room</h1>
        </div>
        <div className="bottom">
        <div className="left">
            <img
              src={
                files
                  ? URL.createObjectURL(files[0])
                 : "https://icon-library.com/images/no-image-icon/no-image-icon-0.jpg"
              }
              alt=""
            />
          </div>
          <div className="right">
            <form>
            <div className="formInput">
                <label htmlFor="file">
                  Image: <DriveFolderUploadOutlinedIcon className="icon" />
                </label>
                <input
                  type="file"
                  id="file"
                  multiple
                  onChange={onFileChange}
                  style={{ display: "none" }}
                />
                <span className="alert-msg">{alert==="" ? "" : `*${alert}`}</span>
              </div>

              <div className="formInput">
                  <label>Title</label>
                  <input
                    id="title"
                    type="text"
                    placeholder="Room name"
                    onChange={(e) => {
                      setInfo((prevState) => {
                        return {
                          ...prevState,
                          name: e.target.value,
                        };
                      });
                    }}
                  />
                </div>
              <div className="formInput">
                  <label>Description</label>
                  <textarea
                    id="desc"
                    type="text-area"
                    cols="60" rows="8"
                    placeholder="Description..."
                    onChange={(e) => {
                      setInfo((prevState) => {
                        return {
                          ...prevState,
                          desc: e.target.value,
                        };
                      });
                    }}

                  ></textarea>
                </div>
              <div className="formInput">
                  <label>Price per night</label>
                  <input
                    id="pricePernight"
                    type="number"
                    placeholder="Price per night"
                    onChange={(e) => {
                      setInfo((prevState) => {
                        return {
                          ...prevState,
                          pricePerNight: e.target.value,
                        };
                      });
                    }}

                  />
                </div>
              <div className="formInput">
                  <label>Guest Capacity</label>
                  <input
                    id="guest-capacity"
                    type="number"
                    placeholder="Guest Capacity"
                    onChange={(e) => {
                      setInfo((prevState) => {
                        return {
                          ...prevState,
                          guestCapacity: e.target.value,
                        };
                      });
                    }}

                  />
                </div>
                
              {/* <div className="formInput">
                <label>Rooms</label>
                <textarea
                  onChange={(e) => setRooms(e.target.value)}
                  placeholder="give comma between room numbers."
                />
              </div> */}
              {/* <div className="formInput">
                <label>Choose a hotel</label>
                <select
                  id="hotelId"
                  onChange={(e) => setHotelId(e.target.value)}
                >
                  {loading
                    ? "loading"
                    : data &&
                      data.map((hotel) => (
                        <option key={hotel._id} value={hotel._id}>{hotel.name}</option>
                      ))}
                </select>
              </div> */}
              <button onClick={handleClick}>Send</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewRoom;
