import React, {useState, useEffect} from 'react'
import axios from 'axios'
import "./editRoom.scss";
import Sidebar from "../../components/sidebar/Sidebar";
import Navbar from "../../components/navbar/Navbar";
import DriveFolderUploadOutlinedIcon from "@mui/icons-material/DriveFolderUploadOutlined";
import useFetch from "../../hooks/useFetch";
import { roomInputs } from "../../formSource";
import { useNavigate, useLocation } from 'react-router-dom';
import swal from "sweetalert";

const EditRoom = () => {
  const [info, setInfo] = useState({});
  const [hotelId, setHotelId] = useState(undefined);
  const [rooms, setRooms] = useState([]);
  const [files, setFiles] = useState("");
  const [alert, setAlert] = useState("")
  const location = useLocation()
  const navigate = useNavigate();
  console.log(location.pathname.split('/')[2])
  const user = JSON.parse(localStorage.getItem("user"))
  const token = user.token
  const [updateData, setUpdateData] = useState({
    name: "",
    desc: "",
    pricePerNight: "",
    guestCapacity: "",
    image: ""
    
  });

  const { data, loading, error } = useFetch(`/room/${location.pathname.split('/')[2]}`);
  console.log(data)
  useEffect(() => {
    setUpdateData({
      name: data.name,
      desc: data.desc,
      pricePerNight: data.pricePerNight,
      guestCapacity: data.guestCapacity,
      photo: data.image
    })
  }, [data]);


  const handleClick = async (e) => {
    e.preventDefault();
    const formData = new FormData()
    formData.append('name', updateData.name) 
    formData.append('desc', updateData.desc) 
    formData.append('pricePerNight', updateData.pricePerNight) 
    formData.append('guestCapacity', updateData.guestCapacity) 
    formData.append('photo', updateData.photo) 
console.log(updateData)
    if(updateData.name === '' || updateData.desc === '' || updateData.pricePerNight === '' || updateData.guestCapacity === '' || updateData.photo === '' ){
      swal({
        title: "Error",
        icon: 'warning',
        text: 'Please fill all required field',
      })
    }
    else{
      
      try {
        const config = {
          headers: {
            'content-type': 'multipart/form-data',
            'authorization': `Bearer ${token}`

          }}
        swal({
          title: "Are you sure?",
          text: "You won't be able to revert this!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then((result)=>{
          if(result){
              axios.put(`http://3.110.167.147/api/room/${location.pathname.split('/')[2]}`, formData, config)
             .then((res) =>{
              swal("Updated!", "Room has been updated.", "success")
              navigate('/room')
             })

          }
        })
      } catch (err) {
        console.log(err);
        swal({
          title: "Error",
          icon: "danger",
          text: "There is an error creating new room",
        })
      }
    }
    
  };

const onFileChange = (e) =>{

  if(e.target.files[0].size < 50000){
    setAlert("")
    setFiles(e.target.files); setUpdateData((prevState)=>{
      return {
        ...prevState,
        photo: e.target.files[0]
      }
    });
  }else{
    setAlert("File size should be less than 5Mb")
  }
   console.log(updateData.photo)

}
  console.log(info)
  return (
    <div className="edit">
      <Sidebar />
      <div className="editContainer">
        <Navbar />
        <div className="top">
          <h1>Edit Room</h1>
        </div>
        <div className="bottom">
        <div className="left">
            <img
              src={
                files
                  ? URL.createObjectURL(files[0])
                 : updateData.photo
              }
              alt=""
            />
          </div>
          <div className="right">
            <form>
              {/* {roomInputs.map((input) => (
                <div className="formInput" key={input.id}>
                  <label>{input.label}</label>
                  <input
                    id={input.id}
                    type={input.type}
                    placeholder={input.placeholder}
                    onChange={handleChange}
                    value={data}
                  />
                </div>
              ))} */}
              <div className="formInput">
                <label htmlFor="file">
                  Image: <DriveFolderUploadOutlinedIcon className="icon" />
                </label>
                <input
                  type="file"
                  id="file"
                  multiple
                  max-size="500"
                  onChange={onFileChange}
                  style={{ display: "none" }}
                />
                <span className="alert-msg">{alert==="" ? "" : `*${alert}`}</span>
              </div>
              <div className="formInput">
                  <label>Title</label>
                  <input
                    id="title"
                    type="text"
                    placeholder="Room name"
                    onChange={(e) => {
                      setUpdateData((prevState) => {
                        return {
                          ...prevState,
                          name: e.target.value,
                        };
                      });
                    }}
                    value={updateData.name}
                  />
                </div>
              <div className="formInput">
                  <label>Description</label>
                  <textarea
                    id="desc"
                    type="text-area"
                    cols="60" rows="8"
                    placeholder="Description..."
                    onChange={(e) => {
                      setUpdateData((prevState) => {
                        return {
                          ...prevState,
                          desc: e.target.value,
                        };
                      });
                    }}
                    value={updateData.desc}
                  ></textarea>
                </div>
              <div className="formInput">
                  <label>Price per night</label>
                  <input
                    id="pricePernight"
                    type="number"
                    placeholder="Price per night"
                    onChange={(e) => {
                      setUpdateData((prevState) => {
                        return {
                          ...prevState,
                          pricePerNight: e.target.value,
                        };
                      });
                    }}
                    value={updateData.pricePerNight}
                  />
                </div>
              <div className="formInput">
                  <label>Guest Capacity</label>
                  <input
                    id="guest-capacity"
                    type="number"
                    placeholder="Guest Capacity"
                    onChange={(e) => {
                      setUpdateData((prevState) => {
                        return {
                          ...prevState,
                          guestCapacity: e.target.value,
                        };
                      });
                    }}
                    value={updateData.guestCapacity}
                  />
                </div>
              {/* <div className="formInput">
                <label>Rooms</label>
                <textarea
                  onChange={(e) => setRooms(e.target.value)}
                  placeholder="give comma between room numbers."
                />
              </div> */}
              {/* <div className="formInput">
                <label>Choose a hotel</label>
                <select
                  id="hotelId"
                  onChange={(e) => setHotelId(e.target.value)}
                >
                  {loading
                    ? "loading"
                    : data &&
                      data.map((hotel) => (
                        <option key={hotel._id} value={hotel._id}>{hotel.name}</option>
                      ))}
                </select>
              </div> */}
              <button onClick={handleClick}>Send</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default EditRoom