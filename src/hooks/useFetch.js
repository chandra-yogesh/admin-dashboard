import { useEffect, useState } from "react";
import apiAxios from "../Axios/api";

const useFetch = (url) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  useEffect(() => {
    
    const fetchData = async () => {
      setLoading(true);
      try {
        const res = await apiAxios.get(url);
        setData(res.data);

      } catch (err) {
        setError(err);
      }
      setLoading(false);
    };
    fetchData();
    console.log(url)
  }, [url]);

  const reFetch = async () => {
    setLoading(true);
    try {
      const res = await apiAxios.get(url);
      setData(res.data);
      console.log(res.data)

    } catch (err) {
      setError(err);
    }
    setLoading(false);
  };

  return { data, loading, error, reFetch };
};

export default useFetch;
